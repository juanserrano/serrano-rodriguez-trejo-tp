package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"

	bolt "github.com/coreos/bbolt"
)

type Cliente struct {
	Nrocliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

type Tarjeta struct {
	Nrotarjeta   int
	Nrocliente   int
	Validadesde  string
	Validahasta  string
	Codseguridad int
	Limitecompra int
	estado       string
}

type Comercio struct {
	Nrocomercio  int
	Nombre       string
	Domicilio    string
	Codigopostal string
	Telefono     string
}

type Compra struct {
	Nrooperacion int
	Nrotarjeta   int
	Nrocomercio  int
	Fecha        string
	Monto        int
	Pagado       bool
}

//...
func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	// abre transacción de escritura
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	// cierra transacción
	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	// abre una transacción de lectura
	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

//...

func main() {
	db, err := bolt.Open("base_nosql.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	juan := Cliente{1, "Juan", "Altamirano", "Juncal 321", "011543652461"}
	cliente1, err := json.Marshal(juan)
	if err != nil {
		log.Fatal(err)
	}

	marcerlo := Cliente{2, "Marcelo", "Torres", "Storni 1427", "341863489245"}
	cliente2, err := json.Marshal(marcerlo)
	if err != nil {
		log.Fatal(err)
	}

	pablo := Cliente{3, "Pablo", "Vera", "Venezuela 85", "011522478154"}
	cliente3, err := json.Marshal(pablo)
	if err != nil {
		log.Fatal(err)
	}

	tarjeta01 := Tarjeta{8001478545968056, 1, "201903", "202101", 3574, 35000.00, "vigente"}
	tarjeta1, err := json.Marshal(tarjeta01)
	if err != nil {
		log.Fatal(err)
	}
	tarjeta02 := Tarjeta{5823096651475125, 2, "201804", "202104", 3581, 75000.00, "vigente"}
	tarjeta2, err := json.Marshal(tarjeta02)
	if err != nil {
		log.Fatal(err)
	}
	tarjeta03 := Tarjeta{3587429330584125, 5, "201908", "202208", 2544, 35000.00, "suspendida"}
	tarjeta3, err := json.Marshal(tarjeta03)
	if err != nil {
		log.Fatal(err)
	}

	comercio01 := Comercio{101, "Autopartes Joaco", "Stopper 3587", "B1616DMP", "011587369524"}
	comercio1, err := json.Marshal(comercio01)
	if err != nil {
		log.Fatal(err)
	}

	comercio02 := Comercio{102, "Libreria Tematika", "Azcuenaga 1345", "B1618EXV", "026145826344"}
	comercio2, err := json.Marshal(comercio02)
	if err != nil {
		log.Fatal(err)
	}

	comercio03 := Comercio{121, "Ferreteria Coco", "Tres Arroyos 2115", "B1619AFD", "1152876358"}
	comercio3, err := json.Marshal(comercio03)
	if err != nil {
		log.Fatal(err)
	}

	compra01 := Compra{1, tarjeta01.Nrotarjeta, 101, "20191101", 1799, true}
	compra1, err := json.Marshal(compra01)
	if err != nil {
		log.Fatal(err)
	}

	compra02 := Compra{2, 5823096651475125, 102, "20191105", 5000, true}
	compra2, err := json.Marshal(compra02)
	if err != nil {
		log.Fatal(err)
	}

	compra03 := Compra{3, 5823096651475125, 121, "20191125", 10000, false}
	compra3, err := json.Marshal(compra03)
	if err != nil {
		log.Fatal(err)
	}

	CreateUpdate(db, "Cliente", []byte(strconv.Itoa(juan.Nrocliente)), cliente1)
	CreateUpdate(db, "Cliente", []byte(strconv.Itoa(marcerlo.Nrocliente)), cliente2)
	CreateUpdate(db, "Cliente", []byte(strconv.Itoa(pablo.Nrocliente)), cliente3)
	CreateUpdate(db, "Tarjeta", []byte(strconv.Itoa(tarjeta01.Nrotarjeta)), tarjeta1)
	CreateUpdate(db, "Tarjeta", []byte(strconv.Itoa(tarjeta02.Nrotarjeta)), tarjeta2)
	CreateUpdate(db, "Tarjeta", []byte(strconv.Itoa(tarjeta03.Nrotarjeta)), tarjeta3)
	CreateUpdate(db, "Comercio", []byte(strconv.Itoa(comercio01.Nrocomercio)), comercio1)
	CreateUpdate(db, "Comercio", []byte(strconv.Itoa(comercio02.Nrocomercio)), comercio2)
	CreateUpdate(db, "Comercio", []byte(strconv.Itoa(comercio03.Nrocomercio)), comercio3)
	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra01.Nrooperacion)), compra1)
	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra02.Nrooperacion)), compra2)
	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra03.Nrooperacion)), compra3)

	resultado, err := ReadUnique(db, "Cliente", []byte(strconv.Itoa(juan.Nrocliente)))
	resultado2, err := ReadUnique(db, "Cliente", []byte(strconv.Itoa(marcerlo.Nrocliente)))
	resultado3, err := ReadUnique(db, "Cliente", []byte(strconv.Itoa(pablo.Nrocliente)))
	resultado4, err := ReadUnique(db, "Tarjeta", []byte(strconv.Itoa(tarjeta01.Nrotarjeta)))
	resultado5, err := ReadUnique(db, "Tarjeta", []byte(strconv.Itoa(tarjeta02.Nrotarjeta)))
	resultado6, err := ReadUnique(db, "Tarjeta", []byte(strconv.Itoa(tarjeta03.Nrotarjeta)))
	resultado7, err := ReadUnique(db, "Comercio", []byte(strconv.Itoa(comercio01.Nrocomercio)))
	resultado8, err := ReadUnique(db, "Comercio", []byte(strconv.Itoa(comercio02.Nrocomercio)))
	resultado9, err := ReadUnique(db, "Comercio", []byte(strconv.Itoa(comercio03.Nrocomercio)))
	resultado10, err := ReadUnique(db, "Compra", []byte(strconv.Itoa(compra01.Nrooperacion)))
	resultado11, err := ReadUnique(db, "Compra", []byte(strconv.Itoa(compra02.Nrooperacion)))
	resultado12, err := ReadUnique(db, "Compra", []byte(strconv.Itoa(compra03.Nrooperacion)))

	fmt.Printf("Clientes:\n %s\n", resultado)
	fmt.Printf("%s\n", resultado2)
	fmt.Printf("%s\n", resultado3)
	fmt.Printf("Tarjetas:\n %s\n", resultado4)
	fmt.Printf("%s\n", resultado5)
	fmt.Printf("%s\n", resultado6)
	fmt.Printf("Comercios:\n %s\n", resultado7)
	fmt.Printf("%s\n", resultado8)
	fmt.Printf("%s\n", resultado9)
	fmt.Printf("Compras:\n %s\n", resultado10)
	fmt.Printf("%s\n", resultado11)
	fmt.Printf("%s\n", resultado12)
}
