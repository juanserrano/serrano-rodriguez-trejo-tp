create function funcion_alertarechazo() returns trigger as $$
		BEGIN
			insert into alerta(nrotarjeta, fecha, nrorechazo, codalerta,descripcion)
			values(new.nrotarjeta,new.fecha, new.nrorechazo, 0 ,new.motivo::text);
			return new;
		END;
		$$ language plpgsql;

		create trigger alertarechazo
		after insert on rechazo
		for each row
		execute procedure funcion_alertarechazo();

create function funcion_alerta2compras1min() returns trigger as $$
		DECLARE
		cantcompras int;
		BEGIN
		select count (distinct (comercio)) into cantcompras from compra,comercio where
		compra.nrocomercio = comercio.nrocomercio and 
		compra.nrotarjeta = new.nrotarjeta and
		codigopostal = (select codigopostal from comercio where nrocomercio = new.nrocomercio) and 
		new.fecha - compra.fecha < '00:01:00';
		if cantcompras is null then 
			cantcompras := 0;
		end if;
		if cantcompras > 0 then 
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
			values (new.nrotarjeta, new.fecha, null, 1, 'Dos compras 1 min mismo CP');
		end if;
		return new;
		END; 
		$$ language plpgsql;
	
	create trigger alerta2compras1min
	BEFORE insert on compra
	for each row
	execute procedure funcion_alerta2compras1min();

create function funcion_alerta2compras5min() returns trigger as $$
		DECLARE
		cantcompras int;
		BEGIN
		select count (*) into cantcompras from compra,comercio where
		compra.nrocomercio = comercio.nrocomercio and 
		compra.nrotarjeta = new.nrotarjeta and
		codigopostal != (select codigopostal from comercio where nrocomercio = new.nrocomercio) and
		new.fecha - compra.fecha < '00:05:00';
		if cantcompras is null then 
			cantcompras := 0;
		end if;
		if cantcompras > 0 then 
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
			values (new.nrotarjeta, new.fecha, null, 1, 'Dos compras 5 min diferente CP');
		end if;
		return new;
		END; 
		$$ language plpgsql;
	
	create trigger alerta2compras5min
	BEFORE insert on compra
	for each row
	execute procedure funcion_alerta2compras5min();

create function funcion_alerta2rechazoslimite() returns trigger as $$
	DECLARE
		ultimorechazo record;
	BEGIN
		select * into ultimorechazo from alerta where  nrotarjeta = new.nrotarjeta and descripcion = 'superalimite de tarjeta' and fecha = (select max(fecha)from alerta) ;
		if (new.fecha - ultimorechazo.fecha < '1 day') THEN
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
			values (new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'Doble rechazo por limite');
			update tarjeta set estado='suspendida' where nrotarjeta = new.nrotarjeta;
		end if;
		return new;
	END;
	$$ language plpgsql;
	
	create trigger alerta2rechazoslimite
	after insert on rechazo
	for each row
	execute procedure funcion_alerta2rechazoslimite();
