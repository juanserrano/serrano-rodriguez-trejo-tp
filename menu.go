package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

func main() {
	menuppal()
}

func menuppal() {
	flag := true
	for flag == true {
		var Opcion int
		fmt.Println("")
		fmt.Println("   Base de datos")
		fmt.Println("")
		fmt.Println(" 1 - Crear base de datos")
		fmt.Println(" 2 - Crear claves")
		fmt.Println(" 3 - Borrar todas las claves")
		fmt.Println(" 4 - Cargar datos")
		fmt.Println(" 5 - Comprar (simulador de compras)")
		fmt.Println(" 6 - Generar Resumenes")
		fmt.Println("")
		fmt.Println(" 9 - Salir")
		fmt.Println("")
		fmt.Printf(" ingrese aqui la opcion deseada:")
		fmt.Scanf("%d", &Opcion)

		if Opcion == 1 {
			creardb()
			creartablas()
			alertarechazo()
			alerta2rechazosporlimite()
			alerta2compras1min()
			alerta2compras5min()
		} else if Opcion == 2 {
			definirclaves()
		} else if Opcion == 3 {
			borrarclaves()
		} else if Opcion == 4 {
			cargardatos()
		} else if Opcion == 5 {
			cargarConsumos()
		} else if Opcion == 6 {
			generaResumen()
		} else if Opcion == 9 {
			flag = false
		} else {
			fmt.Println("Opcion invalida")
		}
	}
}

func creardb() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		`drop database if exists "credito"`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(
		`create database credito`)
	if err != nil {
		log.Fatal(err)
	}
	cargarsp()
}

func creartablas() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		`create table cliente(
		nrocliente      int,
		nombre          text,
		apellido        text,
		domicilio       text,
		telefono        char(12)
	);

	create table tarjeta(
		nrotarjeta      char(16),
		nrocliente      int,
		validadesde     char(6),
		validahasta     char(6),
		codseguridad    char(4),
		limitecompra    decimal(8,2),
		estado          char(10)
	);
	
	create table comercio(
		nrocomercio     int,
		nombre          text,
		domicilio       text,
		codigopostal    char(8),
		telefono        char(12)
	);
	
	create table compra(
		nrooperacion    serial,
		nrotarjeta      char(16),
		nrocomercio     int,
		fecha           timestamp,
		monto           decimal(7,2),
		pagado          boolean
	);
	
	create table rechazo(
		nrorechazo      serial,
		nrotarjeta      char(16),
		nrocomercio     int,
		fecha           timestamp,
		monto           decimal(7,2),
		motivo          text
	);
	
	create table cierre(
		año             int,
		mes             int,
		terminacion     int,
		fechainicio     date,
		fechacierre     date,
		fechavto        date
	);
	
	create table cabecera(
		nroresumen      int,
		nombre          text,
		apellido        text,
		domicilio       text,
		nrotarjeta      char(16),
		desde           date,
		hasta           date,
		vence           date,
		total           decimal(8,2)
	);
	
	create table detalle(
		nroresumen      int,
		nrolinea        int,
		fecha           date,
		nombrecomercio  text,
		monto           decimal(7,2)
	);
	
	create table alerta(
		nroalerta       serial,
		nrotarjeta      char(16),
		fecha           timestamp,
		nrorechazo      int,
		codalerta       int,
		descripcion     text
	);
	
	create table consumo(
		idconsumo		serial,
		nrotarjeta      char(16),
		codseguridad    char(4),
		nrocomercio     int,
		monto           decimal(7,2)
	)`)

	if err != nil {
		log.Fatal(err)
	}
}

func definirclaves() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		`alter table cliente    add constraint cliente_pk   primary key (nrocliente);
		alter table tarjeta     add constraint tarjeta_pk   primary key (nrotarjeta);
		alter table comercio    add constraint comercio_pk  primary key (nrocomercio);
		alter table rechazo     add constraint rechazo_pk   primary key (nrorechazo);
		alter table cierre      add constraint cierre_pk    primary key (año,mes,terminacion);
		alter table cabecera    add constraint cabecera_pk  primary key (nroresumen);
		alter table detalle     add constraint detalle_pk   primary key (nroresumen,nrolinea);
		alter table alerta      add constraint alerta_pk    primary key (nroalerta);
	
		alter table tarjeta     add constraint tarjeta_fk             foreign key (nrocliente)  references cliente(nrocliente);
		alter table compra      add constraint compra_nrotarjeta_fk   foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table compra      add constraint compra_nrocomercio_fk  foreign key (nrocomercio) references comercio(nrocomercio);
		alter table rechazo     add constraint rechazo_nrotarjeta_fk  foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table rechazo     add constraint rechazo_nrocomercio_fk foreign key (nrocomercio) references comercio(nrocomercio);
		alter table cabecera    add constraint nrotarjeta_fk          foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table detalle     add constraint detalle_nroresumen__fk foreign key (nroresumen)  references cabecera(nroresumen);
		alter table alerta      add constraint alerta_nrotarjeta_fk   foreign key (nrotarjeta)  references tarjeta(nrotarjeta);
		alter table alerta      add constraint alerta_nrorechazo_fk   foreign key (nrorechazo)  references rechazo(nrorechazo);
	`)
	if err != nil {
		log.Fatal(err)
	}
}

func borrarclaves() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		`alter table tarjeta    drop constraint tarjeta_fk;
		alter table compra      drop constraint compra_nrocomercio_fk;
		alter table compra 		drop constraint compra_nrotarjeta_fk;
		alter table rechazo     drop constraint rechazo_nrotarjeta_fk;
		alter table rechazo     drop constraint rechazo_nrocomercio_fk;
		alter table cabecera    drop constraint nrotarjeta_fk;
		alter table detalle     drop constraint detalle_nroresumen__fk;
		alter table alerta      drop constraint alerta_nrotarjeta_fk;
		alter table alerta      drop constraint alerta_nrorechazo_fk;

		alter table cliente     drop constraint cliente_pk;
		alter table comercio    drop constraint comercio_pk;
		alter table rechazo     drop constraint rechazo_pk;
		alter table cierre      drop constraint cierre_pk;
		alter table cabecera    drop constraint cabecera_pk;
		alter table detalle     drop constraint detalle_pk;
		alter table alerta      drop constraint alerta_pk;
		alter table tarjeta     drop constraint tarjeta_pk;`)

	if err != nil {
		log.Fatal(err)
	}
}

func cargardatos() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		// Clientes - comercios - Tarjetas - Cierres - Consumos
		`insert into cliente values(1, 'Juan','Altamirano','Juncal 321', '011543652461');
		insert into cliente values(2, 'Marcelo','Torres','Storni 1427', '341863489245');
		insert into cliente values(3, 'Pablo','Vera','Venezuela 85', '011522478154');
		insert into cliente values(4, 'Maria','Salgado','Santa Fe 4500', '037958924557');
		insert into cliente values(5, 'Sandra','Gonzalez','Copernico 967', '036825974138');
		insert into cliente values(6, 'Pedro','Arrascaeta','Dalton 728', '011366895217');
		insert into cliente values(7, 'Veronica','Montes','Av. Directorio 485', '011787852536');
		insert into cliente values(8, 'Margarita','Prieto','Peron 5634', '023204785234');
		insert into cliente values(9, 'Sebastian','Lopez','Coronel Diaz 347', '026135784255');
		insert into cliente values(10, 'Juan','Bogado','Franklin 1212', '024436986536');
		insert into cliente values(11, 'Soledad','Cohen','Lugones 325', '011256365542');
		insert into cliente values(12, 'Martin','Marconi','Gascon 724', '011758488588');
		insert into cliente values(13, 'Gerardo','Ayala','Av. Nestor Kirchner 638', '022347589699');
		insert into cliente values(14, 'Jesica','Flores','Hooke 129', '011456885698');
		insert into cliente values(15, 'Oscar','Silva','Marcos Sastre 1879', '023547584571');
		insert into cliente values(16, 'Marina','Rodriguez','Rivadavia 96', '011358963966');
		insert into cliente values(17, 'Eugenia','Albornoz','11 de septiembre 1888', '011784524821');
		insert into cliente values(18, 'Victor','Serrano','Pichincha 444', '011586428724');
		insert into cliente values(19, 'Fabio','Mitre','Suiza 687', '011965852488');
		insert into cliente values(20, 'Patricia','Morales','Irigoyen 372', '026148526373');
		
		insert into comercio values(101, 'Autopartes Joaco','Stopper 3587','B1645AYF', '011587369524');
		insert into comercio values(102, 'Libreria Tematika','Azcuenaga 1345','B1618EXV', '026145826344');
		insert into comercio values(103, 'Sanitarios Gaona','Av. Gaona 3587','B1615CDU', '011478358742');
		insert into comercio values(104, 'Zapateria Febo','Av. Sarmiento 458','B1609GMH', '026135874521');
		insert into comercio values(105, 'Muebles Woody','San Martin 3471','B1645AYF', '011347825875');
		insert into comercio values(106, 'Tu Bazar','Bouchard 222','B1648FDB', '022347582147');
		insert into comercio values(107, 'Just For Sports','Guemes 4786','B1706BJZ', '011465835874');
		insert into comercio values(108, 'Hesito Pet Shop','Lamadrid 56','B1820ION', '011422125358');
		insert into comercio values(109, 'Corralon los horneros','Genova 357','B1832KTG', '011458769524');
		insert into comercio values(110, 'Joyeria siglo XX','Belgrano 1111','B1678GVS', '011458935783');
		insert into comercio values(112, 'All Print Imprenta','Cangallo 197','B1686MMB', '011458736982');
		insert into comercio values(113, 'Electronorte','Benavidez 2587','B1661BFN', '011549258753');
		insert into comercio values(114, 'Sistem.com Software','Junin 3587','B1666IQD', '011587432481');
		insert into comercio values(115, 'Farmacia central','Saavedra 2547','B1629DPJ', '011452863875');
		insert into comercio values(116, 'Motolandia','Diagonal Norte 358','B2804GDL', '011547832587');
		insert into comercio values(117, 'Eventos del sur','Jujuy 268','B1765AFB', '026134785264');
		insert into comercio values(118, 'Maderas el bosque','Mitre 3879','B1870COF', '011452835899');
		insert into comercio values(121, 'Ferreteria Coco','Tres Arroyos 2115','B1619AFD','1152876358');
		insert into comercio values(119, 'Perfumeria Buenos Aires','Corrientes 662','B1611GCF','011347835874');
		insert into comercio values(120, 'Supermercado DEPASO','Montevideo 1874','B1613FPJ', '026535874458');
		
		insert into tarjeta values ('8001478545968056', 1 , '201903', '202101','3574' ,3500.00, 'vigente');
		insert into tarjeta values ('5823096651475125', 2 , '201804', '202104','3581' ,7500.00, 'vigente');
		insert into tarjeta values ('2478125314778521', 3 , '201905', '202205','2555', 35000.00, 'vigente');
		insert into tarjeta values ('1238964777425842', 4 , '201707', '202007','2587', 3500.00, 'vigente');
		insert into tarjeta values ('3587429330584125', 5 , '201908', '202208','2544', 35000.00, 'suspendida');
		insert into tarjeta values ('3475214412400834', 6 , '201705', '202005','1587', 5000.00, 'vigente');
		insert into tarjeta values ('3478911207896425', 1 , '201703', '202003','6354', 35000.00, 'vigente');
		insert into tarjeta values ('5374285812563278', 7 , '201911', '202211','7895', 3500.00, 'vigente');
		insert into tarjeta values ('1204789605852147', 8 , '201806', '202106','1223', 7500.00, 'anulada');
		insert into tarjeta values ('3358112475234048', 9 , '201612', '201912','1489', 35000.00, 'vigente');
		insert into tarjeta values ('3089124512454513', 10 , '201806', '202106','6537', 7500.00, 'vigente');
		insert into tarjeta values ('3548412450471240', 11 , '201808', '202108','3257', 5000.00, 'vigente');
		insert into tarjeta values ('5984752312412453', 12 , '201705', '202005','4174', 50000.00, 'vigente');
		insert into tarjeta values ('3589871244571235', 13 , '201803', '202103','0020', 35000.00, 'suspendida');
		insert into tarjeta values ('9712121121014102', 15 , '201807', '202107','3520', 50000.00, 'vigente');
		insert into tarjeta values ('6351174125122035', 16 , '201707', '202011','9999', 35000.00, 'vigente');
		insert into tarjeta values ('4241121651281228', 17 , '201802', '202102','4689', 35000.00, 'vigente');
		insert into tarjeta values ('9638712651215441', 18 , '201705', '202005','3333', 5000.00, 'vigente');
		insert into tarjeta values ('7412136844112418', 19 , '201805', '202105','3256', 35000.00, 'anulada');
		insert into tarjeta values ('1213218141383219', 20 , '201611', '201911','6547', 35000.00, 'suspendida');
		insert into tarjeta values ('2854454435418145', 14 , '201809', '202109','9957', 75000.00, 'vigente');
		insert into tarjeta values ('7309764545428126', 14 , '201709', '202009','0123', 3500.00, 'vigente');
		
		insert into cierre values (2019, 1 , 0, '2019-01-01', '2019-02-01', '2019-02-06');
		insert into cierre values (2019, 1 , 1, '2019-01-02', '2019-02-02', '2019-02-07');
		insert into cierre values (2019, 1 , 2, '2019-01-03', '2019-02-03', '2019-02-08');
		insert into cierre values (2019, 1 , 3, '2019-01-04', '2019-02-04', '2019-02-10');
		insert into cierre values (2019, 1 , 4, '2019-01-05', '2019-02-05', '2019-02-11');
		insert into cierre values (2019, 1 , 5, '2019-01-06', '2019-02-06', '2019-02-12');
		insert into cierre values (2019, 1 , 6, '2019-01-07', '2019-02-07', '2019-02-14');
		insert into cierre values (2019, 1 , 7, '2019-01-08', '2019-02-08', '2019-02-15');
		insert into cierre values (2019, 1 , 8, '2019-01-09', '2019-02-09', '2019-02-16');
		insert into cierre values (2019, 1 , 9, '2019-01-10', '2019-02-10', '2019-02-17');
		
		insert into cierre values (2019, 2 , 0, '2019-02-02', '2019-03-02', '2019-03-07');
		insert into cierre values (2019, 2 , 1, '2019-02-03', '2019-03-03', '2019-03-08');
		insert into cierre values (2019, 2 , 2, '2019-02-04', '2019-03-04', '2019-03-09');
		insert into cierre values (2019, 2 , 3, '2019-02-05', '2019-03-05', '2019-03-10');
		insert into cierre values (2019, 2 , 4, '2019-02-06', '2019-03-06', '2019-03-11');
		insert into cierre values (2019, 2 , 5, '2019-02-07', '2019-03-07', '2019-03-12');
		insert into cierre values (2019, 2 , 6, '2019-02-08', '2019-03-08', '2019-03-13');
		insert into cierre values (2019, 2 , 7, '2019-02-09', '2019-03-09', '2019-03-14');
		insert into cierre values (2019, 2 , 8, '2019-02-10', '2019-03-10', '2019-03-15');
		insert into cierre values (2019, 2 , 9, '2019-02-11', '2019-03-11', '2019-03-16');
		
		insert into cierre values (2019, 3 , 0, '2019-03-03', '2019-04-03', '2019-04-08');
		insert into cierre values (2019, 3 , 1, '2019-03-04', '2019-04-04', '2019-04-09');
		insert into cierre values (2019, 3 , 2, '2019-03-05', '2019-04-05', '2019-04-10');
		insert into cierre values (2019, 3 , 3, '2019-03-06', '2019-04-06', '2019-04-11');
		insert into cierre values (2019, 3 , 4, '2019-03-07', '2019-04-07', '2019-04-12');
		insert into cierre values (2019, 3 , 5, '2019-03-08', '2019-04-08', '2019-04-13');
		insert into cierre values (2019, 3 , 6, '2019-03-09', '2019-04-09', '2019-04-14');
		insert into cierre values (2019, 3 , 7, '2019-03-10', '2019-04-10', '2019-04-15');
		insert into cierre values (2019, 3 , 8, '2019-03-11', '2019-04-11', '2019-04-16');
		insert into cierre values (2019, 3 , 9, '2019-03-12', '2019-04-12', '2019-04-17');
		
		insert into cierre values (2019, 4 , 0, '2019-04-04', '2019-05-02', '2019-05-07');
		insert into cierre values (2019, 4 , 1, '2019-04-05', '2019-05-03', '2019-05-08');
		insert into cierre values (2019, 4 , 2, '2019-04-06', '2019-05-04', '2019-05-09');
		insert into cierre values (2019, 4 , 3, '2019-04-07', '2019-05-05', '2019-05-10');
		insert into cierre values (2019, 4 , 4, '2019-04-08', '2019-05-06', '2019-05-11');
		insert into cierre values (2019, 4 , 5, '2019-04-09', '2019-05-07', '2019-05-12');
		insert into cierre values (2019, 4 , 6, '2019-04-10', '2019-05-08', '2019-05-13');
		insert into cierre values (2019, 4 , 7, '2019-04-11', '2019-05-09', '2019-05-14');
		insert into cierre values (2019, 4 , 8, '2019-04-12', '2019-05-10', '2019-05-15');
		insert into cierre values (2019, 4 , 9, '2019-04-13', '2019-05-11', '2019-05-16');
		insert into cierre values (2019, 5 , 0, '2019-05-03', '2019-06-02', '2019-06-07');
		insert into cierre values (2019, 5 , 1, '2019-05-04', '2019-06-03', '2019-06-08');
		insert into cierre values (2019, 5 , 2, '2019-05-05', '2019-06-04', '2019-06-09');
		insert into cierre values (2019, 5 , 3, '2019-05-06', '2019-06-05', '2019-06-10');
		insert into cierre values (2019, 5 , 4, '2019-05-07', '2019-06-06', '2019-06-11');
		insert into cierre values (2019, 5 , 5, '2019-05-08', '2019-06-07', '2019-06-12');
		insert into cierre values (2019, 5 , 6, '2019-05-09', '2019-06-08', '2019-06-13');
		insert into cierre values (2019, 5 , 7, '2019-05-10', '2019-06-09', '2019-06-14');
		insert into cierre values (2019, 5 , 8, '2019-05-11', '2019-06-10', '2019-06-15');
		insert into cierre values (2019, 5 , 9, '2019-05-12', '2019-06-11', '2019-06-16');
		
		insert into cierre values (2019, 6 , 0, '2019-06-03', '2019-07-02', '2019-07-07');
		insert into cierre values (2019, 6 , 1, '2019-06-04', '2019-07-03', '2019-07-08');
		insert into cierre values (2019, 6 , 2, '2019-06-05', '2019-07-04', '2019-07-09');
		insert into cierre values (2019, 6 , 3, '2019-06-06', '2019-07-05', '2019-07-10');
		insert into cierre values (2019, 6 , 4, '2019-06-07', '2019-07-06', '2019-07-11');
		insert into cierre values (2019, 6 , 5, '2019-06-08', '2019-07-07', '2019-07-12');
		insert into cierre values (2019, 6 , 6, '2019-06-09', '2019-07-08', '2019-07-13');
		insert into cierre values (2019, 6 , 7, '2019-06-10', '2019-07-09', '2019-07-14');
		insert into cierre values (2019, 6 , 8, '2019-06-11', '2019-07-10', '2019-07-15');
		insert into cierre values (2019, 6 , 9, '2019-06-12', '2019-07-11', '2019-07-16');
		
		insert into cierre values (2019, 7 , 0, '2019-07-03', '2019-08-02', '2019-08-07');
		insert into cierre values (2019, 7 , 1, '2019-07-04', '2019-08-03', '2019-08-08');
		insert into cierre values (2019, 7 , 2, '2019-07-05', '2019-08-04', '2019-08-09');
		insert into cierre values (2019, 7 , 3, '2019-07-06', '2019-08-05', '2019-08-10');
		insert into cierre values (2019, 7 , 4, '2019-07-07', '2019-08-06', '2019-08-11');
		insert into cierre values (2019, 7 , 5, '2019-07-08', '2019-08-07', '2019-08-12');
		insert into cierre values (2019, 7 , 6, '2019-07-09', '2019-08-08', '2019-08-13');
		insert into cierre values (2019, 7 , 7, '2019-07-10', '2019-08-09', '2019-08-14');
		insert into cierre values (2019, 7 , 8, '2019-07-11', '2019-08-10', '2019-08-15');
		insert into cierre values (2019, 7 , 9, '2019-07-12', '2019-08-11', '2019-08-16');
		
		insert into cierre values (2019, 8 , 0, '2019-08-03', '2019-09-02', '2019-09-03');
		insert into cierre values (2019, 8 , 1, '2019-08-04', '2019-09-03', '2019-09-04');
		insert into cierre values (2019, 8 , 2, '2019-08-05', '2019-09-04', '2019-09-05');
		insert into cierre values (2019, 8 , 3, '2019-08-06', '2019-09-05', '2019-09-06');
		insert into cierre values (2019, 8 , 4, '2019-08-07', '2019-09-06', '2019-09-07');
		insert into cierre values (2019, 8 , 5, '2019-08-08', '2019-09-07', '2019-09-08');
		insert into cierre values (2019, 8 , 6, '2019-08-09', '2019-09-08', '2019-09-09');
		insert into cierre values (2019, 8 , 7, '2019-08-10', '2019-09-09', '2019-09-10');
		insert into cierre values (2019, 8 , 8, '2019-08-11', '2019-09-10', '2019-09-11');
		insert into cierre values (2019, 8 , 9, '2019-08-12', '2019-09-11', '2019-09-12');
		
		insert into cierre values (2019, 9 , 0, '2019-09-03', '2019-10-02', '2019-10-07');
		insert into cierre values (2019, 9 , 1, '2019-09-04', '2019-10-03', '2019-10-08');
		insert into cierre values (2019, 9 , 2, '2019-09-05', '2019-10-04', '2019-10-09');
		insert into cierre values (2019, 9 , 3, '2019-09-06', '2019-10-05', '2019-10-10');
		insert into cierre values (2019, 9 , 4, '2019-09-07', '2019-10-06', '2019-10-11');
		insert into cierre values (2019, 9 , 5, '2019-09-08', '2019-10-07', '2019-10-12');
		insert into cierre values (2019, 9 , 6, '2019-09-09', '2019-10-08', '2019-10-13');
		insert into cierre values (2019, 9 , 7, '2019-09-10', '2019-10-09', '2019-10-04');
		insert into cierre values (2019, 9 , 8, '2019-09-11', '2019-10-10', '2019-10-05');
		insert into cierre values (2019, 9 , 9, '2019-09-12', '2019-10-11', '2019-10-06');
		
		insert into cierre values (2019, 10 , 0, '2019-10-03', '2019-11-02', '2019-11-07');
		insert into cierre values (2019, 10 , 1, '2019-10-04', '2019-11-03', '2019-11-08');
		insert into cierre values (2019, 10 , 2, '2019-10-05', '2019-11-04', '2019-11-09');
		insert into cierre values (2019, 10 , 3, '2019-10-06', '2019-11-05', '2019-11-10');
		insert into cierre values (2019, 10 , 4, '2019-10-07', '2019-11-06', '2019-11-11');
		insert into cierre values (2019, 10 , 5, '2019-10-08', '2019-11-07', '2019-11-12');
		insert into cierre values (2019, 10 , 6, '2019-10-09', '2019-11-08', '2019-11-13');
		insert into cierre values (2019, 10 , 7, '2019-10-10', '2019-11-09', '2019-11-14');
		insert into cierre values (2019, 10 , 8, '2019-10-11', '2019-11-10', '2019-11-15');
		insert into cierre values (2019, 10 , 9, '2019-10-12', '2019-11-11', '2019-11-16');
		
		insert into cierre values (2019, 11 , 0, '2019-11-03', '2019-12-02', '2019-12-07');
		insert into cierre values (2019, 11 , 1, '2019-11-04', '2019-12-03', '2019-12-08');
		insert into cierre values (2019, 11 , 2, '2019-11-05', '2019-12-04', '2019-12-09');
		insert into cierre values (2019, 11 , 3, '2019-11-06', '2019-12-05', '2019-12-10');
		insert into cierre values (2019, 11 , 4, '2019-11-07', '2019-12-06', '2019-12-11');
		insert into cierre values (2019, 11 , 5, '2019-11-08', '2019-12-07', '2019-12-12');
		insert into cierre values (2019, 11 , 6, '2019-11-09', '2019-12-08', '2019-12-13');
		insert into cierre values (2019, 11 , 7, '2019-11-10', '2019-12-09', '2019-12-14');
		insert into cierre values (2019, 11 , 8, '2019-11-11', '2019-12-10', '2019-12-15');
		insert into cierre values (2019, 11 , 9, '2019-11-12', '2019-12-11', '2019-12-16');
		
		insert into cierre values (2019, 12 , 0, '2019-12-03', '2020-01-02', '2020-01-07');
		insert into cierre values (2019, 12 , 1, '2019-12-04', '2020-01-03', '2020-01-08');
		insert into cierre values (2019, 12 , 2, '2019-12-05', '2020-01-04', '2020-01-09');
		insert into cierre values (2019, 12 , 3, '2019-12-06', '2020-01-05', '2020-01-10');
		insert into cierre values (2019, 12 , 4, '2019-12-07', '2020-01-06', '2020-01-11');
		insert into cierre values (2019, 12 , 5, '2019-12-08', '2020-01-07', '2020-01-12');
		insert into cierre values (2019, 12 , 6, '2019-12-09', '2020-01-08', '2020-01-13');
		insert into cierre values (2019, 12 , 7, '2019-12-10', '2020-01-09', '2020-01-14');
		insert into cierre values (2019, 12 , 8, '2019-12-11', '2020-01-10', '2020-01-15');
		insert into cierre values (2019, 12 , 9, '2019-12-12', '2020-01-11', '2020-01-16');
		
		insert into consumo (nrotarjeta,codseguridad,nrocomercio,monto)
		values
		('8001478545968056','3574',105, 3600.00),
		('8001478545968056','3574',107, 300.00),
		('8001478545968056','3574',104, 200.00),
		('8001478545968056','3574',108, 100.00),
		('8001478545968056','3574',108, 100.00),
		('8001478545968056','1255',105, 2500.00),
		('8001478545968056','3574',105, 80.00),
		('8001478545968056','3574',105, 50.00),
		('8001478545968056','3574',101, 250.00),
		('8001478545968056','1435',112, 100.00),
		('8001478545968056','3574',118, 300.00),
		('8001478545968056','3574',115, 400.00),

		('5823096651475125','3581',101 ,1500.00),
		('5823096651475125','3581',103 ,500.00),
		('5823096651475125','3581',107 ,80.00),
		('5823096651475125','3111',105 ,150.00),
		('5823096651475125','3581',117 ,100.00),

		('2478125314778521','2555',118, 100.00),
		('2478125314778521','2555',112, 320.00),
		('2478125314778521','2555',108, 180.00),
		('2478125314778521','2555',105, 199.00),

		('1238964777425842','2587',107, 341.00),
		('1238964777425842','2587',109, 192.00),
		('1238964777425842','2587',110, 500.00),
		('1238964777425842','2587',113, 180.00),
		('1238964777425842','4587',120, 5000.00),
		('1238964777425842','2587',119, 100.00),
		('1238964777425842','2587',115, 987.00),

		('3587429330584125','2544',101, 400.00),

		('1238964777425842','2587',103, 1500.00),
		('1238964777425842','2587',108, 358.00),
		('1238964777425842','2587',105, 180.00),

		('3478911207896425','1247',108, 192.00),
		('3478911207896425','6354',112, 8000.00),
		('3478911207896425','6354',113, 349.00),
		('3478911207896425','6354',117, 100.00),
		('3478911207896425','6354',115, 387.50),
		('3478911207896425','6354',108, 358.00),
		('3478911207896425','6354',104, 341.00),

		('5374285812563278','7895',103, 2500.00),
		('5374285812563278','7895',103, 1500.00),

		('1204789605852147','1223',108, 1800.00),
		('1204789605852147','1223',105, 7150.00),
		('1204789605852147','1223',102, 7500.00),
		
		('3358112475234048','1489',108, 347.00),
		('3358112475234048','2587',109, 358.00),
		('3358112475234048','1489',107, 4560.00),
		('3358112475234048','1489',104, 380.00),
		('3358112475234048','1489',105, 397.00),
		('3358112475234048','1489',113, 4800.00),
		('3358112475234048','1489',117, 800.00),
		('3358112475234048','1489',114, 750.00),

		('3089124512454513','6537',119, 187.00),
		('3089124512454513','6537',114, 30.00),
		('3089124512454513','6537',101, 398.00),

		('3548412450471240','3257',108, 358.00),

		('5984752312412453','4174',109, 1000.00),
		('5984752312412453','4174',112, 1830.00),

		('3589871244571235','0020',118, 358.00),
		('3589871244571235','0020',115, 358.00),
		('3589871244571235','0020',120, 190.00),
		('3589871244571235','0020',118, 369.00),

		('9712121121014102','3520',101, 3698.00),
		('9712121121014102','3520',102, 1000.00),
		('9712121121014102','3520',107, 100.00),
		('9712121121014102','3520',107, 800.00),
		('9712121121014102','3520',104, 59.00),

		('6351174125122035','9999',119, 682.00),

		('9638712651215441','3333',114, 600.00),
		('9638712651215441','3333',115, 180.00),
		('9638712651215441','3333',101, 975.00),
		('9638712651215441','3333',103, 369.00),
		('9638712651215441','3333',108, 180.00),

		('7412136844112418','3256',106, 1500.00),
		('7412136844112418','3256',104, 258.00),
		('7412136844112418','3256',106, 1600.00),

		('1213218141383219','6547',107, 479.00),
		('1213218141383219','6547',119, 369.00),
		('1213218141383219','6547',120, 450.00),
		('1213218141383219','6547',114, 1000.00),

		('2854454435418145','9957',108, 180.00),
		('2854454435418145','9957',105, 259.00),
		('2854454435418145','9957',108, 700.00),
		('2854454435418145','9957',104, 750.00),

		('7309764545428126','0123',103, 1080.00),
		('7309764545428126','0123',102, 600.00),
		('7309764545428126','0123',104, 1500.00),
		('7309764545428126','0123',115, 600.00)`)
	if err != nil {
		log.Fatal(err)
	}
}

func cargarConsumos() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		`create function cargarconsumos() returns void as $$
	DECLARE
		f consumo%rowtype;
		r record;
	BEGIN 
		for f in select * from consumo loop
			select * into r from comprar(f.idconsumo);
		end loop;
	END;
	$$ language plpgsql;
	select cargarconsumos();`)

	if err != nil {
		log.Fatal(err)
	}
}

func generaResumen() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	_, err = db.Exec(
		`create function todoslosresumenes() returns void as $$ 
		DECLARE
			c cliente%rowtype;
			r1 record;
		BEGIN
			for c in select * from cliente  loop
				select * into r1 from generaresumen(c.nrocliente, 2019, 11);
			end loop;
		END;
		$$ language plpgsql;
		select todoslosresumenes();`)
	if err != nil {
		log.Fatal(err)
	}
}

// --------Triggers--------------------------------------------------------------------------------

func alertarechazo() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(
		`create function funcion_alertarechazo() returns trigger as $$
		BEGIN
			insert into alerta(nrotarjeta, fecha, nrorechazo, codalerta,descripcion)
			values(new.nrotarjeta,new.fecha, new.nrorechazo, 0 ,new.motivo::text);
			return new;
		END;
		$$ language plpgsql;

		create trigger alertarechazo
		after insert on rechazo
		for each row
		execute procedure funcion_alertarechazo();`)
	if err != nil {
		log.Fatal(err)
	}
}

func alerta2compras1min() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(
		`create function funcion_alerta2compras1min() returns trigger as $$
		DECLARE
		cantcompras int;
		BEGIN
		select count (distinct (comercio)) into cantcompras from compra,comercio where
		compra.nrocomercio = comercio.nrocomercio and 
		compra.nrotarjeta = new.nrotarjeta and
		codigopostal = (select codigopostal from comercio where nrocomercio = new.nrocomercio) and 
		new.fecha - compra.fecha < '00:01:00';
		if cantcompras is null then 
			cantcompras := 0;
		end if;
		if cantcompras > 0 then 
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
			values (new.nrotarjeta, new.fecha, null, 1, 'Dos compras 1 min mismo CP');
		end if;
		return new;
		END; 
		$$ language plpgsql;
	
	create trigger alerta2compras1min
	BEFORE insert on compra
	for each row
	execute procedure funcion_alerta2compras1min();`)
	if err != nil {
		log.Fatal(err)
	}
}

func alerta2compras5min() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(
		`create function funcion_alerta2compras5min() returns trigger as $$
		DECLARE
		cantcompras int;
		BEGIN
		select count (*) into cantcompras from compra,comercio where
		compra.nrocomercio = comercio.nrocomercio and 
		compra.nrotarjeta = new.nrotarjeta and
		codigopostal != (select codigopostal from comercio where nrocomercio = new.nrocomercio) and
		new.fecha - compra.fecha < '00:05:00';
		if cantcompras is null then 
			cantcompras := 0;
		end if;
		if cantcompras > 0 then 
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
			values (new.nrotarjeta, new.fecha, null, 1, 'Dos compras 5 min diferente CP');
		end if;
		return new;
		END; 
		$$ language plpgsql;
	
	create trigger alerta2compras5min
	BEFORE insert on compra
	for each row
	execute procedure funcion_alerta2compras5min();`)
	if err != nil {
		log.Fatal(err)
	}
}

func alerta2rechazosporlimite() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(
		`create function funcion_alerta2rechazoslimite() returns trigger as $$
	DECLARE
		ultimorechazo record;
	BEGIN
		select * into ultimorechazo from alerta where  nrotarjeta = new.nrotarjeta and descripcion = 'superalimite de tarjeta' and fecha = (select max(fecha)from alerta) ;
		if (new.fecha - ultimorechazo.fecha < '1 day') THEN
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
			values (new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'Doble rechazo por limite');
			update tarjeta set estado='suspendida' where nrotarjeta = new.nrotarjeta;
		end if;
		return new;
	END;
	$$ language plpgsql;
	
	create trigger alerta2rechazoslimite
	after insert on rechazo
	for each row
	execute procedure funcion_alerta2rechazoslimite();
		`)
	if err != nil {
		log.Fatal(err)
	}
}

// -------------Stored Procedures --------------------------------------------------------------------
func cargarsp() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=credito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	_, err = db.Exec(
		`create function comprar(idc int) returns void as $BODY$
    DECLARE
        consumox record;
        autorizado boolean;
    BEGIN
        select * into consumox from consumo where idconsumo=idc;
        if autorizacion(consumox.nrotarjeta,consumox.codseguridad, consumox.nrocomercio,consumox.monto) then
            insert into compra(nrotarjeta, nrocomercio, fecha, monto, pagado)
            VALUES
			(consumox.nrotarjeta, consumox.nrocomercio, current_timestamp, consumox.monto, false);
        end if;
    END;
    $BODY$ LANGUAGE plpgsql;


		create function autorizacion(ntarjeta char(16),codseg char(4), ncomercio int, nuevomonto decimal(7,2)) returns boolean as $$ 
		DECLARE 
			respuesta boolean;
		BEGIN
			respuesta:=true;
			if not estarjetavalida(ntarjeta) then
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) 
				values (ntarjeta, ncomercio, current_timestamp, nuevomonto, 'tarjeta no valida o no vigente');
				respuesta := respuesta and false;
			end if;
			if not escodigocorrecto(ntarjeta, codseg) then
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) 
				values (ntarjeta, ncomercio, current_timestamp, nuevomonto, 'codigo de seguridad invalido');
				respuesta := respuesta and false;
			end if;
			if superalimite(ntarjeta,nuevomonto) then
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) 
				values (ntarjeta, ncomercio, current_timestamp, nuevomonto, 'superalimite de tarjeta');
				respuesta := respuesta and false;
			end if;
			if not noestavencida(ntarjeta) then
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) 
				values (ntarjeta, ncomercio, current_timestamp, nuevomonto, 'plazo de vigencia expirado');
				respuesta := respuesta and false;
			end if;
			if estasuspendida(ntarjeta) then
				insert into rechazo(nrotarjeta, nrocomercio, fecha, monto, motivo) 
				values (ntarjeta, ncomercio, current_timestamp, nuevomonto, 'la tarjeta se encuentra suspendida');
				respuesta := respuesta and false;
			end if;
			return respuesta;	
		END;
		$$ language plpgsql;
	

		create function estarjetavalida(ntarjeta char(16)) returns boolean as $$
    DECLARE
        respuesta boolean;
        resultado record;
    BEGIN
        respuesta := true;
        select * into resultado from tarjeta where nrotarjeta = ntarjeta and estado = 'vigente';
        if not found THEN
            respuesta := false;
        end if;  
        return respuesta;
    END;
	$$ language plpgsql;


	create function escodigocorrecto(ntarjeta char(16), codseg char(4)) returns boolean as $$ 
    DECLARE
        respuesta boolean;
        resultado record;
    BEGIN
        respuesta := false;
        select * into resultado from tarjeta where nrotarjeta=ntarjeta and codseguridad=codseg;
        if found THEN
            respuesta :=true;
        end if;
        return respuesta;
    END;
    $$ language plpgsql;
	
	create function superalimite(ntarjeta char(16), nuevomonto decimal(7,2)) returns boolean as $$
    DECLARE   
		respuesta boolean;
        lim decimal(8,2);
        deuda decimal(8,2);    
	BEGIN
        respuesta := false;
        select limitecompra into lim from tarjeta where nrotarjeta = ntarjeta;
		select sum(monto) into deuda from compra where pagado = false and nrotarjeta = ntarjeta;
		if deuda is null THEN
			deuda:= 0.00;
		end if;
		deuda := nuevomonto + deuda;
        if deuda > lim THEN
            respuesta := true;
        end if;
        return respuesta;
    END;
    $$ language plpgsql;
	
	create function noestavencida(ntarjeta char(16)) returns boolean as $$
    DECLARE
        respuesta boolean;
        resultado record;
    BEGIN
        respuesta := true;
        select * into resultado from tarjeta where nrotarjeta = ntarjeta and to_date(validahasta,'YYYYMM')> current_date;
        if not found THEN
            respuesta := false;
        end if;  
        return respuesta;
    END;
	$$ language plpgsql;

	create function estasuspendida(ntarjeta char(16)) returns boolean as $$
    DECLARE
        respuesta boolean;
        resultado record;
    BEGIN
        respuesta := false;
        select * into resultado from tarjeta where nrotarjeta=ntarjeta and estado='suspendida';
        if found then
            respuesta := true;
        end if;
        return respuesta;
    END;
	$$ language plpgsql;
	
	create function generaresumen(ncliente int, a int, m int) returns void as $$ 
	DECLARE
		datospersonales record;
		ntarjeta char(16);
		infocierre record;
		numresumen int;
		iterador int;
		tot decimal(7,2);
		r record;		
   BEGIN
		iterador:=0; 
		tot :=0.0;
		select count (*) into numresumen from cabecera;
		if numresumen is null then
			numresumen:=0;
		end if;
		select * into datospersonales from cliente where nrocliente = ncliente;

		--select * from tarjeta where nrocliente = ncliente;

		select nrotarjeta into ntarjeta from tarjeta where nrocliente = ncliente;
		select * into infocierre from cierre where cierre.año=a and cierre.mes=m and terminacion = right(ntarjeta,1)::int;	
		insert into cabecera values(numresumen, datospersonales.nombre, datospersonales.apellido, datospersonales.domicilio, ntarjeta, infocierre.fechainicio, infocierre.fechacierre, infocierre.fechavto,0.0);
		for r in select * from comercio,compra where comercio.nrocomercio = compra.nrocomercio and fecha::date >=infocierre.fechainicio and fecha::date < infocierre.fechacierre and nrotarjeta = ntarjeta loop
     		insert into detalle values(numresumen, iterador, r.fecha, r.nombre, r.monto);
         	tot = tot + r.monto;
			iterador = iterador + 1;
		  end loop;	
		  update cabecera set total=tot where nroresumen=numresumen;
   END;
$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
}
