create function generaresumen(ncliente int, a int, m int) returns void as $$ 
	DECLARE
		datospersonales record;
		ntarjeta char(16);
		infocierre record;
		numresumen int;
		iterador int;
		tot decimal(7,2);
		r record;		
   BEGIN
		iterador:=0; 
		tot :=0.0;
		select count (*) into numresumen from cabecera;
		if numresumen is null then
			numresumen:=0;
		end if;
		select * into datospersonales from cliente where nrocliente = ncliente;
		select nrotarjeta into ntarjeta from tarjeta where nrocliente = ncliente;
		select * into infocierre from cierre where cierre.año=a and cierre.mes=m and terminacion = right(ntarjeta,1)::int;	
		insert into cabecera values(numresumen, datospersonales.nombre, datospersonales.apellido, datospersonales.domicilio, ntarjeta, infocierre.fechainicio, infocierre.fechacierre, infocierre.fechavto,0.0);
		for r in select * from comercio,compra where comercio.nrocomercio = compra.nrocomercio and fecha::date >=infocierre.fechainicio and fecha::date < infocierre.fechacierre and nrotarjeta = ntarjeta loop
     		insert into detalle values(numresumen, iterador, r.fecha, r.nombre, r.monto);
         	tot = tot + r.monto;
			iterador = iterador + 1;
		  end loop;	
		  update cabecera set total=tot where nroresumen=numresumen;
   END;
$$ language plpgsql;`)
