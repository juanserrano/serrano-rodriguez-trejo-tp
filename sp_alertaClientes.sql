create function funcion_alertarechazo() returns trigger as $$
    DECLARE
    BEGIN
        insert into alerta values(select numalerta(),new.nrotarjeta,new.fecha,new.nrorechazo,0,'rechazo');
    END;
    $$ language plpgsql;

create trigger alertarechazo
after of insert on rechazo
for each row
execute procedure funcion_alertarechazo();


create function funcion_alertadoscompras_a() returns trigger as $$
    DECLARE
        ultimacompra record;
        ultimocodpostal char(8);
    BEGIN
        select codigopostal into ultimocodpostal from comercio where 
        nrocomercio=new.nrocomercio;
        select max(fecha) into ultimacompra from comercio where nrocomercio!=new.nrocomercio
        and nrocomercio in(select nrocomercio from comercio where codigopostal=ultimocodigopostal);
        if (new.fecha - ultimacompra.fecha < '1 minute') THEN
            insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
            values(new.nrotarjeta, new.fecha, new.nrorechazo, 1, 
            'Dos compras en comercios distintos dentro del mismo código postal');
        end if;
    END;
    $$ language plpgsql;

create trigger alertadoscompras_a
instead of insert on compra
for each row
execute procedure funcion_alertadoscompras_a();

create function funcion_alertadoscompras_b() returns trigger as $$
    DECLARE
        ultimacompra record;
        ultimocodpostal char(8);
    BEGIN
        select codigopostal into ultimocodpostal from comercio where 
        nrocomercio=new.nrocomercio;
        select max(fecha) into ultimacompra from compra where nrocomercio in
        (select nrocomercio from comercio where codigopostal!=ultimocodpostal);
        if (new.fecha - ultimacompra.fecha < '5 minutes') THEN
            insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
            values(new.nrotarjeta, new.fecha, new.nrorechazo, 5,
            'Dos compras en menos de 5 minutos en comercios con
            distintos códigos postales');
    END;
    $$ language plpgsql;

create trigger alertadoscompras_b
before insert on compra
for each row
execute procedure funcion_alertadoscompras_b();

create function numalerta() returns int as $$ 
    DECLARE
        n int;
    BEGIN
		$$ language plpgsql;
	
		create trigger alertarechazo
		after of insert on rechazo
		for each row
		execute procedure funcion_alertarechazo();
        select count(*) into n from alerta;
        return n;
    END;
    $$ language plpgsql;


create function funcion_alerta2rechazoslimite() returns trigger as $$
    DECLARE
        ultimorechazo record;
    BEGIN
        select max(fecha) into ultimorechazo from alerta where nrotarjeta = new.nrotarjeta and descripcion = 'superalimite de tarjeta';
        if (new.fecha - ultimorechazo.fecha < '1 day') THEN
            insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
            values (new.nrotarjeta, new.fecha, new,nrorechazo, 32, 'Doble rechazo por limite');
            update tarjeta set estado='suspendida' where nrotarjeta = new.nrotarjeta;
        end if;
    END;
    $$ language plpgsql;

create trigger alerta2rechazoslimite
BEFORE of insert on rechazo
for each row
execute procedure funcion_alerta2rechazoslimite();



create function funcion_alertarechazo() returns trigger as $$
    DECLARE
    BEGIN
        insert into alerta values(select numalerta(),new.nrotarjeta,new.fecha,new.nrorechazo,0,'rechazo');
    END;
    $$ language plpgsql;

create trigger alertarechazo
after of insert on rechazo
for each row
execute procedure funcion_alertarechazo();


create function funcion_alertadoscompras_a() returns trigger as $$
    DECLARE
        ultimacompra record;

    BEGIN
        select max(fecha) into ultimacompra from compra, comercio;
        select extract (epoch from (new.fecha - ultimacompra.fecha));
    END;
    $$ language plpgsql;

create trigger alertadoscompras_a
instead of insert on compra
for each row
execute procedure funcion_alertadoscompras_a();



create function numalerta() returns int as $$ 
    DECLARE
        n int;
    BEGIN
		$$ language plpgsql;
	
		create trigger alertarechazo
		after of insert on rechazo
		for each row
		execute procedure funcion_alertarechazo();
        select count(*) into n from alerta;
        return n;
    END;
    $$ language plpgsql;


create function funcion_alerta2rechazoslimite() returns trigger as $$
    DECLARE
        ultimorechazo record;
    BEGIN
        select max(fecha) into ultimorechazo from alerta where nrotarjeta = new.nrotarjeta and descripcion = 'superalimite de tarjeta';
        if (new.fecha - ultimorechazo.fecha < '1 day') THEN
            insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
            values (new.nrotarjeta, new.fecha, new,nrorechazo, 32, 'Doble rechazo por limite');
            update tarjeta set estado='suspendida' where nrotarjeta = new.nrotarjeta;
        end if;
    END;
    $$ language plpgsql;

create trigger alerta2rechazoslimite
BEFORE of insert on rechazo
for each row
execute procedure funcion_alerta2rechazoslimite();



create function funcion_alerta2compras1min() return trigger as $$
    DECLARE
    cantcompras int;
    BEGIN
    select count (distinct (comercio)) from compra,comercio where
    compra.nrocomercio = comercio.nrocomercio and 
    compra.nrotarjeta = new.nrotarjeta and
    codigopostal = new.codigopostal and 
    new.fecha - compra.fecha < '00:01:00';
    if cantcompras is null then 
        cantcompras := 0;
    end if;
    if cantcompras > 0 then 
        insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
        values (new.nrotarjeta, new.fecha, null, 1, 'Dos compras 1 min mismo CP');
    end if;
    return cantcompras;
    END; 
    $$ language plpgsql;

create trigger alerta2compras1min
BEFORE insert on compra
for each row
execute procedure funcion_alerta2compras1min();




create function funcion_alerta2compras5min() return trigger as $$
    DECLARE
    cantcompras int;
    BEGIN
    select count * from compra,comercio where
    compra.nrocomercio = comercio.nrocomercio and 
    compra.nrotarjeta = new.nrotarjeta and
    codigopostal != new.codigopostal and 
    new.fecha - compra.fecha < '00:05:00';
    if cantcompras is null then 
        cantcompras := 0;
    end if;
    if cantcompras > 0 then 
        insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion)
        values (new.nrotarjeta, new.fecha, null, 1, 'Dos compras 5 min diferente CP');
    end if;
    return cantcompras;
    END; 
    $$ language plpgsql;

create trigger alerta2compras5min
BEFORE insert on compra
for each row
execute procedure funcion_alerta5compras1min();

create function funcion_alerta2rechazoslimite() returns trigger as $$
	DECLARE
		ultimorechazo record;
	BEGIN
		select * into ultimorechazo from alerta where  nrotarjeta = new.nrotarjeta and descripcion = 'superalimite de tarjeta' and fecha = (select max(fecha)from alerta) ;
		if (new.fecha - ultimorechazo.fecha < '1 day') THEN
			insert into alerta (nrotarjeta, fecha, nrorechazo, codalerta, descripcion) 
			values (new.nrotarjeta, new.fecha, new.nrorechazo, 32, 'Doble rechazo por limite');
			update tarjeta set estado='suspendida' where nrotarjeta = new.nrotarjeta;
		end if;
		return new;
	END;
	$$ language plpgsql;
	
	create trigger alerta2rechazoslimite
	after insert on rechazo
	for each row
	execute procedure funcion_alerta2rechazoslimite();
